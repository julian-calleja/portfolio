---
title: Data Processing Architecture - Kappa Architecture
date: 2021-07-25T10:00:00.000+00:00
slug: kappa-architecture
description: An overview of different data processing architectures in 2021
draft: true
keywords:
- Architecture
tags: ["data", "architecture", "realtime", "batch", "kappa", "lambda"]
math: true
toc: false

---
### Kappa Architecture

[Jay Kreps](https://mobile.twitter.com/jaykreps?lang=en) (the founder of Apache Kafka) introduced the Kappa architecture. The Kappa architecture is a software architecture for processing streaming data. 

This architecture was created as an alternative to the Lambda architecture. It is simpler alternative as it uses the same same technology stack to handle both the real-time and batch processing.  As a result, this architecture is based  around the idea of using a single stream processing engine to handle data processing. 

**Raw data** has to be stored in an **immutable data storage system**. The streaming process will then read the raw data from this data store and carry out the transformations. 

Apache Kafka is one of the most widely utilized technologies for serving as the primary data store. Kafka allows us to have a sequence of events with timestamps. Retention periods can be set to store raw data for a specified amount of time. If we want to make sure we have the raw data for the last 30 days, we can tell Kafka to preserve it for that long. This is a crucial  feature because if we need to reprocess any data, we can go back to the beginning of the topic and reprocess the data again.

Once the data is available the streaming job will take care of transforming this data. Open source tools such as **Apache NiFi**, **Spark**, **Flink** etc can be utilized to transform the data. Then the output of the pipeline can then be stored into a database so that it is nice and easy to get the data and create views. 

When we to update the job due to either requirement change or a bug was identified the following process can be adopted:

- The **job** is **updated** 
- Once the job is updated we can start **reading** from the **beginning** of the **topic** and start **reprocessing** the data
- A new set of tables can be created so that the new version of the data is stored. This means that during this time we will have two jobs processing the data
    - This provides us with the ability to test and validate that the new version of the job, if the new version is working as expected the old job can be switched off if not, the old version can still be used 
- Once the new version of the job is **validated** the application can be switched to **start reading from the new table** 
