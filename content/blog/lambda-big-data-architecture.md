---
title: Data Processing Architecture - Lambda Architecture
date: 2021-07-23T10:00:00.000+00:00
slug: lambda-architecture
description: An overview of different data processing architectures in 2021
draft: false
keywords:
- Architecture
tags: ["data", "architecture", "realtime", "batch", "kappa", "lambda"]
math: true
toc: false

---
### Lambda Architecture

The Lambda Architecture was created by [Nathan Marz](https://twitter.com/nathanmarz) based on his experience working on distributed data processing systems at Backtype and Twitter. The following are the primary goals that this architecture seeks to achieve:

- *Robust*
- *Fault-tolerant* (both against hardware and human failures)
- Serving a *variety of workloads* and use cases with *low-latency reads*
- *Linearly scalable*; ie. being able to horizontally scale rather than vertically scale

To meet these requirements, this architecture combines a traditional batch pipeline with a fast real-time stream pipeline for data access.

![Lambda Architecture](/img/lambda_architecture_2.png "Lambda Architecture")

This system can be broken down into five distinct components, which are as follows:

1. **Data Sources**
   - These are the various data sources that we will use for analysis. This source is typically a streaming source, such as *Apache Kafka*, which serves as an intermediary data store that can *serve* both the *batch* and *speed* layers. The data is delivered in parallel to both data sources.
2. **Batch Layer**
   - In this component, all incoming data is processed as batch views in preparation for indexing. All transactions in these components will be recorded as a series of changes/updates, resulting in a dataset similar to that produced by a *change data capture system* ([**CDC**](https://en.wikipedia.org/wiki/Change_data_capture)). As a result, the data in this layer will be considered *immutable* and *append-only*. This will assist us in achieving a reliable historical record of all incoming data.
3. **Serving Layer** 
   - This layer is in charge of *indexing* the most recent batch views so that end users can query them. It also allows you to re-index all of the data in order to help fix a possible bug or to create different indexes that can be used in different use cases. The primary requirement for this layer to work is that all of the processing is done in a *parallelized* way to reduce the time required to index the data set. 
4. **Speed Layer**
	- This layer works hand-in-hand with the serving layer by indexing the most recently added data that has not yet been indexed by the serving layer. Thus in this layer, we will have the data that the serving layer is currently indexing as well as the new data that has arrived after the indexing job has started. Thus this layer allows us to *minimize the lag gap* between the time the latest data was added to the system and the time the latest data is available for querying. 
	- Stream processing tools are normally used to help index the incoming data in near real-time to help minimize the latency for data querying. 
5. **Query**
	- This is the component that the end-user will make use of. It is responsible for *submitting* the queries requested by the end-user to the speed layer and the serving layer and once these two layers provide their *distinct* results then they need to be *consolidated* into a *single dataset* to provide the final result. Thus this gives the end-user the full picture using all of the data, including the most recently added data combined with historical data.  

### But how does this Architecture work?
As previously stated, both the Batch and Speed layers work in tandem, but how do they do so?

Seeing as batch indexing is time-consuming, the speed layer can help by indexing all new unindexed data in near real-time. When the batch job completes, the newly batch-indexed data will be ready for querying. This means that the speed layer's copy of the same data is no longer required, and it will be deleted.

### Benefits and Drawbacks of the Lambda Architecture
#### Pros
- The batch layer of this architecture handles historical data while taking advantage of fault-tolerant distributed storage, which helps ensure a low possibility of errors even if the system crashes.
- It offers a good balance of speed and reliability.

#### Cons
- While the batch layer and the speed layer are dealing with different issues, their internal processing logic is the same. This means that we must maintain two codebases in this architecture, which can be time and resource consuming.


### Resources
[O'Reilly](https://www.oreilly.com/radar/questioning-the-lambda-architecture/)
[Hazelcast](https://hazelcast.com/glossary/lambda-architecture/)
[Lambda-Architecture.net](http://lambda-architecture.net/)
