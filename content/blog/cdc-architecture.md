---
title: The quickest & easiest way to get started with CDC
date: 2021-09-13T10:00:00.000+00:00
slug: postgresql-cdc
description:
draft: false
keywords:
- Architecture
tags: ["data", "realtime", "cdc", "postgresql", "kafka", "debezium"]
math: true
toc: false

---

## The quickest & easiest way to get started with CDC

In this blog, we will go through the concept of **CDC** (Change Data Capture). **Debezium**, **PostgreSQL**, and **Kafka** will be the services used to achieve this goal. 


### Docker Setup 
This docker-compose file is going to spin up the following services:

- PostgreSQL DB 
- Zookeeper
- Single Node Kafka Cluster
- Kafka Connect

These are the bare minimum services that we will use to have a proof of concept working by the end of this blog.

```
version: '3.9'

services:
  db:
    image: postgres:latest
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_PASSWORD=test123
    command:
      - "postgres"
      - "-c"
      - "wal_level=logical"

  zookeeper:
    image: debezium/zookeeper
    ports:
      - "2181:2181"
      - "2888:2888"
      - "3888:3888"

  kafka:
    image: debezium/kafka
    ports:
      - "9092:9092"
      - "29092:29092"
    depends_on:
      - zookeeper
    environment:
      - ZOOKEEPER_CONNECT=zookeeper:2181
      - KAFKA_ADVERTISED_LISTENERS=LISTENER_EXT://localhost:29092,LISTENER_INT://kafka:9092
      - KAFKA_LISTENER_SECURITY_PROTOCOL_MAP=LISTENER_INT:PLAINTEXT,LISTENER_EXT:PLAINTEXT
      - KAFKA_LISTENERS=LISTENER_INT://0.0.0.0:9092,LISTENER_EXT://0.0.0.0:29092
      - KAFKA_INTER_BROKER_LISTENER_NAME=LISTENER_INT

  connect:
    image: debezium/connect
    ports:
      - "8083:8083"
    environment:
      - BOOTSTRAP_SERVERS=kafka:9092
      - GROUP_ID=1
      - CONFIG_STORAGE_TOPIC=my_connect_configs
      - OFFSET_STORAGE_TOPIC=my_connect_offsets
      - STATUS_STORAGE_TOPIC=my_connect_statuses
    depends_on:
      - zookeeper
      - kafka

```
_Note_: This docker-compose file is for testing purposes. The wal_level needs to be updated from the default replica to logical. This will allow us to use the replication feature provided out of the box by PostgreSQL. This feature will allow us to capture transactions using the WAL (Write Ahead Log).

Run the docker-compose.yml file using the following command:

```
docker-compose up
```

Now that the services are up we can start configuring them to start capturing updates from PostgreSQL. 

### Setting up PostgreSQL

#### Creating the sample DVD Rental Database
Let's use the famous PostgreSQL DVD Rental sample database for this tutorial. This can be achieved by restoring the database using the following commands:

1. Download the database dump from the following [link]().
2. Use the `docker exec -it <postgresql_container_id> bash` to enter in the container
3. Create a directory on the container. In this example we created the directory '/data'
4. Exit from the container
5. Change the directory to the location where the database dump is stored
6. Use the `docker cp dvdrental.tar <postgresql_container_id>:/data` to copy the .tar file onto the container 
7. Enter into the container once again using the previous docker exec command
8. Use the `psql -U postgres` to open an SQL shell
9. Create a database `create database dvdrental` and exit the shell
10. Change the directory to '/data'
11. Use `pg_restore -U postgres -d dvdrental dvdrental.tar`

#### Installing the wal2json plugin
After the database dump has been restored, we can begin configuring the `wal2json` plugin. This plugin can be installed by following the steps below:

1. Use the `docker exec -it <postgresql_container_id> bash` to enter in the container
2. Now that we are inside the container we need to execute the following commands:

````
apt-get update && apt-get install postgresql-13-wal2json
````

### Activating Debezium 

Now we are ready to activate Debezium!

An HTTP request allows us to communicate with Debezium. We must send a POST request with the connector configuration in JSON format. In this example, we will set the test connector's properties as follows.

```
echo '
{
    "name": "dvdrental-connector-1",
    "config": {
        "connector.class": "io.debezium.connector.postgresql.PostgresConnector",
        "tasks.max": "1",
        "plugin.name": "wal2json",
        "database.hostname": "db",
        "database.port": "5432",
        "database.user": "postgres",
        "database.password": "arctype",
        "database.dbname": "dvdrental",
        "database.server.name": "DVDRENTAL",
        "key.converter": "org.apache.kafka.connect.json.JsonConverter",
        "value.converter": "org.apache.kafka.connect.json.JsonConverter",
        "key.converter.schemas.enable": "false",
        "value.converter.schemas.enable": "false",
        "snapshot.mode": "always",
        "slot.name": "dvdrental_1"
    }
}
' > dvdrental_debezium_connector.json
```

Once we have this file, we can create the dvd-rental-connector-1 by executing the following POST request.

```
 curl -i -X POST \
         -H "Accept:application/json" \
         -H "Content-Type:application/json" \
         127.0.0.1:8083/connectors/ \
         --data "@dvdrental_debezium_connector.json"
```

This should then yield the following response:

```
HTTP/1.1 201 Created  
Date: Tue, 27 Jul 2021 12:09:26 GMT  
Location: http://127.0.0.1:8083/connectors/dvdrental-connector-1  
Content-Type: application/json  
Content-Length: 640  
Server: Jetty(9.4.33.v20201020)  
  
{"name":"dvdrental-connector-1","config":{"connector.class":"io.debezium.connector.postgresql.PostgresConnector","tasks.max":"1","pl  
ugin.name":"wal2json","database.hostname":"db","database.port":"5432","database.user":"postgres","database.password":"arctype","data  
base.dbname":"dvdrental","database.server.name":"DVDRENTAL","key.converter":"org.apache.kafka.connect.json.JsonConverter","value.con  
verter":"org.apache.kafka.connect.json.JsonConverter","key.converter.schemas.enable":"false","value.converter.schemas.enable":"false  
","snapshot.mode":"always","slot.name":"dvdrental_1","name":"dvdrental-connector-1"},"tasks":[],"type":"source"}
```

By querying the following endpoint, we can check the connector's health status:

```
curl http://localhost:8083/connectors/dvdrental-connector-1/status
```

Response:
```
{"name":"dvdrental-connector-1","connector":{"state":"RUNNING","worker_id":"172.23.0.5:8083"},"tasks":[{"id":0,"state":"RUNNING","wo  
rker_id":"172.23.0.5:8083"}],"type":"source"}%
```

### Testing the Kafka Straming Setup

Now we are streaming! This means that now with every INSERT, UPDATE or DELETE we will get an event. These events are then sent to Kafka. 

Note: Kafka Connect will create a topic per SQL table.  To verify that everything is working, we can check that we have all the topics created per table.

Use the following Kafka commands to see the streamed data:

```
./kafka-topics.sh --bootstrap-server localhost:29092 --list
```

We can also check that the inserts have been captured and found in the created in their respective Kafka topics:

```
./kafka-console-consumer.sh --bootstrap-server localhost:29092 --topic DVDRENTAL.public.city --from-beginning
```


### Conclusion

We were able to construct a CDC system so that transactions could be transformed into events, as we've seen throughout this blog. These events can then be consumed into other Kafka-compatible systems.
