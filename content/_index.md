---
heading: Hi, I'm Julian
subheading: Working as a data engineer for the past couple of years. Although I usually work on data projects, this hasn't stopped me from venturing out. Please don't hesitate to contact me if you have an idea and wish to work together.
handle: julian-calleja

---
