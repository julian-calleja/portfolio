---
title: "Overview"
date: 2021-07-01T00:00:00-00:00
slug: "overview"
description: ""
keywords: []
draft: false
tags: []
math: false
toc: true
---

## Experience
### GiG
- Implemented an *end-to-end data integrity Test Automation Framework* targeting various **real-time data streams** such as Customer, Casino, and Payments. A test suite of around **300+ tests** was implemented to assess critical paths covering *market legislation requirements* and *SLAs*; This project was the first within the whole company to help measure and quantify the system quality which helped in the process of instilling more confidence in the product.
- Aided Big Data Engineers in the process of *developing real-time streams* using **NiFi**. This helped in the process of ensuring that quality is one of the **main pillars of the product**.
- Established a data reconciliation framework to aid BI Developers in the process of reconciling data between the data warehouse and the visualization tool (Qlik Sense).  **1.5K+ checks** have also been developed checking for data freshness, completeness, and semantic checks.
- Contributed to the development of new features of the internal Test Automation ecosystem. Overhauled the Reporting service UI to help the stakeholders visualize and understand the results in a more effective way.
- Initiated several *POCs* associated with **data quality**, **data integrity**, and **performance testing** for GiG Data.
- *Mentored* a new team member through *various concepts* linked to **Data Engineering** and **Automation Testing**.
- *Presented* the testing efforts done for GiG Data to a **company-wide audience**.

*Technologies*: C# (.NET Core), Java, Python, Apache Kafka, RabbitMQ, NiFi, ClickHouse, Qlik, Docker, CI/CD, Kubernetes, ELK Stack, Specflow, GIT, TeamCity, JIRA

### Bit8
- Aided in the development and maintenance of an in-house Test Automation Framework based on Selenium. The latter aided in the identification of User Experience issues, while assisting manual testers. This allowed for an **decrease** in the **release cycle** by approximately **2 days**.
- Formed part of a number of Test Plan creating meetings with both development teams and business to devise test plans targeting the correct functionality while making sure that testing efforts were maximized.

*Technologies*: C# (.NET Framework), Specflow, Selenium, SVN, Bamboo, JIRA

### RS2 
- Helped develop a number of JIRA plugins in order to help automate day-to-day business processes such as the automated creation of on-boarding tickets on different project in JIRA.
- Formed part of a **data migration** project related to JIRA. This process required a substantial amount of research on different processes and techniques that could be used to carry out this migration. The results were then presented to the project team lead and infrastructure engineers for review. Once the processes were reviewed the migration was carried out, resulting in the **consolidation** of *four instances* into a *single JIRA server*.

*Technologies*: Groovy, Java, Vagrant, CentOS, JIRA

## Education
### L-Università ta' Malta / University of Malta [Visiting Student]
- ICS5110 - Applied Machine Learning
- ICS5111 - Mining and Visualizing Large-Scale Data
  
### L-Università ta' Malta / University of Malta [B.Sc.IT in Software Development]
### [Google Cloud](https://www.qwiklabs.com/public_profiles/5437404b-d8b2-42c7-a114-495a254b2268)
- Data Engineering
- Baseline: Data, ML, AI
- Engineer Data in Google Cloud
- BigQuery for Data Warehousing
- Perform Foundational Data, ML, and AI Tasks in Google Cloud
- Developing Data and Machine Learning Apps with C#
- Create and Manage Cloud Resources
