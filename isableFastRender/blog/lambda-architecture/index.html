<!DOCTYPE html>
<html lang="en-us">

<head><script src="/livereload.js?port=1313&mindelay=10&v=2" data-no-instant defer></script>
  <title>Data Processing Architecture - Lambda Architecture | Julian Calleja</title>

  <meta charset="UTF-8">
  <meta name="language" content="en">
  <meta name="description" content="An overview of different data processing architectures in 2021">
  <meta name="keywords" content="Architecture">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  
  

  <link rel="shortcut icon" type="image/png" href="/favicon.ico" />

  
  
    
 
  
  
  
  
  
  
    
    <link type="text/css" rel="stylesheet" href="/css/post.min.679ea46872c2b319c5ae942c601a7330924fd57451c89d621d6cb129c870c2a8.css" integrity="sha256-Z56kaHLCsxnFrpQsYBpzMJJP1XRRyJ1iHWyxKchwwqg="/>
  
    
    <link type="text/css" rel="stylesheet" href="/css/custom.min.cc3beb78ffe8ac221d4dd13d706263cc640cd72bbe4831420068936e65ffae66.css" integrity="sha256-zDvreP/orCIdTdE9cGJjzGQM1yu&#43;SDFCAGiTbmX/rmY="/>
  
  
   
   
    

<script type="application/ld+json">
  
    {
      "@context" : "http://schema.org",
      "@type" : "BlogPosting",
      "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "http:\/\/localhost:1313\/"
      },
      "articleSection" : "blog",
      "name" : "Data Processing Architecture - Lambda Architecture",
      "headline" : "Data Processing Architecture - Lambda Architecture",
      "description" : "An overview of different data processing architectures in 2021",
      "inLanguage" : "en-US",
      "author" : "",
      "creator" : "",
      "publisher": "",
      "accountablePerson" : "",
      "copyrightHolder" : "",
      "copyrightYear" : "2021",
      "datePublished": "2021-07-23 10:00:00 \x2b0000 \x2b0000",
      "dateModified" : "2021-07-23 10:00:00 \x2b0000 \x2b0000",
      "url" : "http:\/\/localhost:1313\/blog\/lambda-architecture\/",
      "wordCount" : "664",
      "keywords" : ["Architecture", "Blog"]
    }
  
  </script>
</head>

<body>
  <div class="burger__container">
  <div class="burger" aria-controls="navigation" aria-label="Menu">
    <div class="burger__meat burger__meat--1"></div>
    <div class="burger__meat burger__meat--2"></div>
    <div class="burger__meat burger__meat--3"></div>
  </div>
</div>
 

  <nav class="nav" role="navigation">
  <ul class="nav__list">
    
    
      <li>
        <a  href="/">welcome</a>
      </li>
    
      <li>
        <a  href="/about/overview/">about</a>
      </li>
    
      <li>
        <a  class="active"
         href="/blog">blog</a>
      </li>
    
      <li>
        <a  href="/resources">resources</a>
      </li>
    
  </ul>
</nav>


  <main>
    
    

    <div class="flex-wrapper">
      <div class="post__container">
        <div class="post">
          <header class="post__header">
            <h1 id="post__title">Data Processing Architecture - Lambda Architecture</h1>
            <time datetime="2021-07-23 10:00:00 &#43;0000 &#43;0000" class="post__date"
            >Jul 23 2021</time>
          </header>
          <article class="post__content">
              
<h3 id="lambda-architecture">Lambda Architecture<a class="anchor" href="#lambda-architecture">#</a></h3>
<p>The Lambda Architecture was created by <a href="https://twitter.com/nathanmarz">Nathan Marz</a> based on his experience working on distributed data processing systems at Backtype and Twitter. The following are the primary goals that this architecture seeks to achieve:</p>
<ul>
<li><em>Robust</em></li>
<li><em>Fault-tolerant</em> (both against hardware and human failures)</li>
<li>Serving a <em>variety of workloads</em> and use cases with <em>low-latency reads</em></li>
<li><em>Linearly scalable</em>; ie. being able to horizontally scale rather than vertically scale</li>
</ul>
<p>To meet these requirements, this architecture combines a traditional batch pipeline with a fast real-time stream pipeline for data access.</p>
<p><img src="/img/lambda_architecture_2.png" alt="Lambda Architecture" title="Lambda Architecture"></p>
<p>This system can be broken down into five distinct components, which are as follows:</p>
<ol>
<li><strong>Data Sources</strong>
<ul>
<li>These are the various data sources that we will use for analysis. This source is typically a streaming source, such as <em>Apache Kafka</em>, which serves as an intermediary data store that can <em>serve</em> both the <em>batch</em> and <em>speed</em> layers. The data is delivered in parallel to both data sources.</li>
</ul>
</li>
<li><strong>Batch Layer</strong>
<ul>
<li>In this component, all incoming data is processed as batch views in preparation for indexing. All transactions in these components will be recorded as a series of changes/updates, resulting in a dataset similar to that produced by a <em>change data capture system</em> (<a href="https://en.wikipedia.org/wiki/Change_data_capture"><strong>CDC</strong></a>). As a result, the data in this layer will be considered <em>immutable</em> and <em>append-only</em>. This will assist us in achieving a reliable historical record of all incoming data.</li>
</ul>
</li>
<li><strong>Serving Layer</strong>
<ul>
<li>This layer is in charge of <em>indexing</em> the most recent batch views so that end users can query them. It also allows you to re-index all of the data in order to help fix a possible bug or to create different indexes that can be used in different use cases. The primary requirement for this layer to work is that all of the processing is done in a <em>parallelized</em> way to reduce the time required to index the data set.</li>
</ul>
</li>
<li><strong>Speed Layer</strong>
<ul>
<li>This layer works hand-in-hand with the serving layer by indexing the most recently added data that has not yet been indexed by the serving layer. Thus in this layer, we will have the data that the serving layer is currently indexing as well as the new data that has arrived after the indexing job has started. Thus this layer allows us to <em>minimize the lag gap</em> between the time the latest data was added to the system and the time the latest data is available for querying.</li>
<li>Stream processing tools are normally used to help index the incoming data in near real-time to help minimize the latency for data querying.</li>
</ul>
</li>
<li><strong>Query</strong>
<ul>
<li>This is the component that the end-user will make use of. It is responsible for <em>submitting</em> the queries requested by the end-user to the speed layer and the serving layer and once these two layers provide their <em>distinct</em> results then they need to be <em>consolidated</em> into a <em>single dataset</em> to provide the final result. Thus this gives the end-user the full picture using all of the data, including the most recently added data combined with historical data.</li>
</ul>
</li>
</ol>
<h3 id="but-how-does-this-architecture-work">But how does this Architecture work?<a class="anchor" href="#but-how-does-this-architecture-work">#</a></h3>
<p>As previously stated, both the Batch and Speed layers work in tandem, but how do they do so?</p>
<p>Seeing as batch indexing is time-consuming, the speed layer can help by indexing all new unindexed data in near real-time. When the batch job completes, the newly batch-indexed data will be ready for querying. This means that the speed layer&rsquo;s copy of the same data is no longer required, and it will be deleted.</p>
<h3 id="benefits-and-drawbacks-of-the-lambda-architecture">Benefits and Drawbacks of the Lambda Architecture<a class="anchor" href="#benefits-and-drawbacks-of-the-lambda-architecture">#</a></h3>
<h4 id="pros">Pros</h4>
<ul>
<li>The batch layer of this architecture handles historical data while taking advantage of fault-tolerant distributed storage, which helps ensure a low possibility of errors even if the system crashes.</li>
<li>It offers a good balance of speed and reliability.</li>
</ul>
<h4 id="cons">Cons</h4>
<ul>
<li>While the batch layer and the speed layer are dealing with different issues, their internal processing logic is the same. This means that we must maintain two codebases in this architecture, which can be time and resource consuming.</li>
</ul>
<h3 id="resources">Resources<a class="anchor" href="#resources">#</a></h3>
<p><a href="https://www.oreilly.com/radar/questioning-the-lambda-architecture/">O&rsquo;Reilly</a>
<a href="https://hazelcast.com/glossary/lambda-architecture/">Hazelcast</a>
<a href="http://lambda-architecture.net/">Lambda-Architecture.net</a></p>


              
                  

<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_SVG"></script>
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
            showMathMenu: false, //disables context menu
            tex2jax: {
            inlineMath: [ ['$','$'], ['\\(','\\)'] ]
           }
    });
</script>
              
          </article>
          

<ul class="tags__list">
    
    <li class="tag__item">
        <a class="tag__link" href="http://localhost:1313/tags/data/">data</a>
    </li>
    <li class="tag__item">
        <a class="tag__link" href="http://localhost:1313/tags/architecture/">architecture</a>
    </li>
    <li class="tag__item">
        <a class="tag__link" href="http://localhost:1313/tags/realtime/">realtime</a>
    </li>
    <li class="tag__item">
        <a class="tag__link" href="http://localhost:1313/tags/batch/">batch</a>
    </li>
    <li class="tag__item">
        <a class="tag__link" href="http://localhost:1313/tags/kappa/">kappa</a>
    </li>
    <li class="tag__item">
        <a class="tag__link" href="http://localhost:1313/tags/lambda/">lambda</a>
    </li></ul>

 <div class="pagination">
  

  
</div>

          
          <footer class="post__footer">
            


<div class="social-icons">
  
     
    
  
     
    
  
     
    
      <a class="social-icons__link" title="Email"
         href="mailto:jcall0098@gmail.com"
         target="_blank" rel="noopener">
        <div class="social-icons__icon" style="background-image: url('http://localhost:1313/svg/email.svg')"></div>
      </a>
    
  
     
    
  
     
    
      <a class="social-icons__link" title="GitLab"
         href="https://gitlab.com/julian-calleja"
         target="_blank" rel="noopener">
        <div class="social-icons__icon" style="background-image: url('http://localhost:1313/svg/gitlab.svg')"></div>
      </a>
    
  
     
    
  
     
    
      <a class="social-icons__link" title="LinkedIn"
         href="https://www.linkedin.com/in/julian-calleja/"
         target="_blank" rel="noopener">
        <div class="social-icons__icon" style="background-image: url('http://localhost:1313/svg/linkedin.svg')"></div>
      </a>
    
  
     
    
     
</div>

            <p>© 2021</p>
          </footer>
          </div>
      </div>
      
    </div>
    

  </main>

   

  
  <script src="/js/index.min.49e4d8a384357d9b445b87371863419937ede9fa77737522ffb633073aebfa44.js" integrity="sha256-SeTYo4Q1fZtEW4c3GGNBmTft6fp3c3Ui/7YzBzrr&#43;kQ=" crossorigin="anonymous"></script>
  
  
  <script src="https://unpkg.com/prismjs@1.20.0/components/prism-core.min.js"></script>

  
  <script src="https://unpkg.com/prismjs@1.20.0/plugins/autoloader/prism-autoloader.min.js"
    data-autoloader-path="https://unpkg.com/prismjs@1.20.0/components/"></script>

  


</body>

</html>
